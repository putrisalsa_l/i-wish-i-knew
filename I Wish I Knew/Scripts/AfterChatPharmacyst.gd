extends Node2D

export (String) var sceneName = ''

func _process(delta):
	if global.change_scene_livingroom_night:
		$AnimationPlayer.play("Fade")
		yield($AnimationPlayer, "animation_finished")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == 'Fade':
		get_tree().change_scene(str('res://Scenes/' + sceneName + '.tscn'))
		global.change_scene_livingroom_night = false
