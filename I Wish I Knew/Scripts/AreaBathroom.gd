extends Area2D

var button_active = false
var dialogue_done_shower

func _on_AreaBathroom_body_entered(body):
	if body.get_name() == 'Chris':
		button_active = true

func _on_AreaBathroom_body_exited(body):
	if body.get_name() == 'Chris':
		button_active = false

func _input(event):
	if Input.is_action_just_pressed('interact') and button_active:
		global.dialogue_on = true
		button_active = false
		dialogue_done_shower = Dialogic.start('done shower')
		add_child(dialogue_done_shower)
		dialogue_done_shower.connect('timeline_end', self, 'dialogue_end')

func _process(delta):
	$"interactButton".visible = button_active

func dialogue_end(timeline_name):
	global.dialogue_on = false
