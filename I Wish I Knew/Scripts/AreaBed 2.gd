extends Area2D

var button_active = false
var dialogue_cant_sleep_again

func _on_AreaBed_body_entered(body):
	if body.get_name() == 'Chris':
		button_active = true

func _on_AreaBed_body_exited(body):
	if body.get_name() == 'Chris':
		button_active = false

func _input(event):
	if Input.is_action_just_pressed('interact') and button_active:
		global.dialogue_on = true
		button_active = false
		dialogue_cant_sleep_again = Dialogic.start('cant sleep again')
		add_child(dialogue_cant_sleep_again)
		dialogue_cant_sleep_again.connect('timeline_end', self, 'dialogue_end')

func _process(delta):
	$"interactButton".visible = button_active

func dialogue_end(timeline_name):
	global.dialogue_on = false
