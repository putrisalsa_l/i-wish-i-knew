extends Area2D

var button_active = false
var dialogue_turnoff_lamp
var dialogue_cant_sleep_again

func _on_AreaBed_body_entered(body):
	if body.get_name() == 'Chris':
		button_active = true
	if global.chris_invisible:
		button_active = false

func _on_AreaBed_body_exited(body):
	if body.get_name() == 'Chris':
		button_active = false

func _input(event):
	if Input.is_action_just_pressed('interact') and button_active and global.lamp_is_off:
		button_active = false
		$bedWithChris.visible = true
		global.chris_invisible = true
		global.playscene_gate_opened = true

	elif Input.is_action_just_pressed('interact') and button_active and !global.lamp_is_off:
		global.dialogue_on = true
		dialogue_turnoff_lamp = Dialogic.start('turn off lamp')
		add_child(dialogue_turnoff_lamp)
		dialogue_turnoff_lamp.connect('timeline_end', self, 'dialogue_end')

func _process(delta):
	$"interactButton".visible = button_active

func dialogue_end(timeline_name):
	global.dialogue_on = false
