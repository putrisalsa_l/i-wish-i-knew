extends Area2D

var button_active = false
var cannot_interact = false

func _on_AreaBookshelf_body_entered(body):
	if body.get_name() == 'Chris':
		button_active = true
	if cannot_interact:
		button_active = false

func _on_AreaBookshelf_body_exited(body):
	if body.get_name() == 'Chris':
		button_active = false

func _input(event):
	if Input.is_action_just_pressed('interact') and button_active:
		button_active = false
		cannot_interact = true
		global.put_books = true

func _process(delta):
	$"interactButton".visible = button_active
