extends Area2D

var button_active = false
var curtain_opened = true

func _on_AreaCurtain_body_entered(body):
	if body.get_name() == 'Chris':
		button_active = true
	if global.chris_invisible:
		button_active = false

func _on_AreaCurtain_body_exited(body):
	if body.get_name() == 'Chris':
		button_active = false

func _input(event):
	if Input.is_action_just_pressed('interact') and button_active and curtain_opened:
		$curtainClose.visible = true
		$curtainOpen.visible = false
		curtain_opened = false

	elif Input.is_action_just_pressed('interact') and button_active and !curtain_opened:
		$curtainClose.visible = false
		$curtainOpen.visible = true
		curtain_opened = true

func _process(delta):
	$"interactButton".visible = button_active
