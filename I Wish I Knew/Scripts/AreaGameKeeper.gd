extends Area2D

var button_active = false
var dialogue_hint_gamekeeper

func _on_AreaGameKeeper_body_entered(body):
	if body.get_name() == 'Chris':
		button_active = true

func _on_AreaGameKeeper_body_exited(body):
	if body.get_name() == 'Chris':
		button_active = false

func _input(event):
	if Input.is_action_just_pressed('interact') and button_active:
		global.dialogue_on = true
		button_active = false
		dialogue_hint_gamekeeper = Dialogic.start('hint gamekeeper')
		add_child(dialogue_hint_gamekeeper)
		dialogue_hint_gamekeeper.connect('timeline_end', self, 'dialogue_end')

func _process(delta):
	$"interactButton".visible = button_active

func dialogue_end(timeline_name):
	global.dialogue_on = false
	global.ask_gamekeeper = true
