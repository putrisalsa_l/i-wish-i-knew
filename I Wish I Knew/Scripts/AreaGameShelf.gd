extends Area2D

var button_active = false
var dialogue_buy
var dialogue_ask

func _on_AreaGameShelf_body_entered(body):
	if body.get_name() == 'Chris':
		button_active = true
	if global.game_taken:
		button_active = false

func _on_AreaGameShelf_body_exited(body):
	if body.get_name() == 'Chris':
		button_active = false

func _input(event):
	if Input.is_action_just_pressed('interact') and button_active and !global.ask_gamekeeper:
		global.dialogue_on = true
		button_active = false
		dialogue_ask = Dialogic.start('ask gamekeeper')
		add_child(dialogue_ask)
		dialogue_ask.connect('timeline_end', self, 'dialogue_end')
		
	elif Input.is_action_just_pressed('interact') and button_active and global.ask_gamekeeper:
		global.dialogue_on = true
		global.game_taken = true
		button_active = false
		dialogue_buy = Dialogic.start('buy cavecraft')
		add_child(dialogue_buy)
		dialogue_buy.connect('timeline_end', self, 'dialogue_end')

func _process(delta):
	$"interactButton".visible = button_active

func dialogue_end(timeline_name):
	global.dialogue_on = false
