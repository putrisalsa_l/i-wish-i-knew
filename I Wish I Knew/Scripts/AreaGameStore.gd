extends Area2D

export (String) var sceneName = ''
var button_active = false

func _on_AreaGameStore_body_entered(body):
	if body.get_name() == 'Chris':
		button_active = true

func _on_AreaGameStore_body_exited(body):
	if body.get_name() == 'Chris':
		button_active = false

func _input(event):
	if Input.is_action_just_pressed('interact') and button_active:
		button_active = false
		get_tree().change_scene(str('res://Scenes/' + sceneName + '.tscn'))

func _process(delta):
	$"interactButton".visible = button_active
