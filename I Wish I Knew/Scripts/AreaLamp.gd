extends Area2D

var button_active = false

func _on_AreaLamp_body_entered(body):
	if body.get_name() == 'Chris' and !global.playscene_wanna_sleep:
		button_active = true
	if global.chris_invisible:
		button_active = false

func _on_AreaLamp_body_exited(body):
	if body.get_name() == 'Chris':
		button_active = false

func _input(event):
	if Input.is_action_just_pressed('interact') and button_active and !global.lamp_is_off:
		$Light2D.enabled = false
		global.lamp_is_off = true
	elif Input.is_action_just_pressed('interact') and button_active and global.lamp_is_off:
		$Light2D.enabled = true
		global.lamp_is_off = false

func _process(delta):
	$"interactButton".visible = button_active
