extends Area2D

var label_active = false
var button_disabled = true
var locked = false

func _on_AreaLockButton_body_entered(body):
	if body.get_name() == 'Chris':
		label_active = true
		button_disabled = false

func _on_AreaLockButton_body_exited(body):
	if body.get_name() == 'Chris':
		label_active = false
		button_disabled = true

func _on_UnlockButton_pressed():
	locked = true
	global.door_locked = true
	$AnimationPlayer.play("Door locked")

func _on_LockButton_pressed():
	locked = false
	global.door_locked = false
	$AnimationPlayer.play("Door unlocked")

func _process(delta):
	if locked:
		$Locked.visible = label_active
		$LockButton.visible = true
		$LockButton.disabled = button_disabled

		$Unlocked.visible = false
		$UnlockButton.visible = false
	else:
		$Locked.visible = false
		$LockButton.visible = false

		$Unlocked.visible = label_active
		$UnlockButton.visible = true
		$UnlockButton.disabled = button_disabled
