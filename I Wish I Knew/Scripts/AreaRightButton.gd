extends Area2D

var label_active = false
var button_disabled = true

func _on_AreaRightButton_body_entered(body):
	if body.get_name() == 'Chris':
		label_active = true
		button_disabled = false

func _on_AreaRightButton_body_exited(body):
	if body.get_name() == 'Chris':
		label_active = false
		button_disabled = true

func _on_TextureButton_pressed():
	global.change_scene_livingroom_morning = true

func _process(delta):
	$Label.visible = label_active
	$TextureButton.disabled = button_disabled
