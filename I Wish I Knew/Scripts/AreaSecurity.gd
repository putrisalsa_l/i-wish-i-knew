extends Area2D

var button_active = false
var dialogue_hint_security
var dialogue_security_taskdone

func _on_AreaSecurity_body_entered(body):
	if body.get_name() == 'Chris':
		button_active = true

func _on_AreaSecurity_body_exited(body):
	if body.get_name() == 'Chris':
		button_active = false

func _input(event):
	if Input.is_action_just_pressed('interact') and button_active and !global.game_taken:
		global.dialogue_on = true
		button_active = false
		dialogue_hint_security = Dialogic.start('hint security')
		add_child(dialogue_hint_security)
		dialogue_hint_security.connect('timeline_end', self, 'dialogue_end')

	elif Input.is_action_just_pressed('interact') and button_active and global.game_taken:
		global.dialogue_on = true
		button_active = false
		dialogue_security_taskdone = Dialogic.start('security task done')
		add_child(dialogue_security_taskdone)
		dialogue_security_taskdone.connect('timeline_end', self, 'dialogue_end')

func _process(delta):
	$"interactButton".visible = button_active

func dialogue_end(timeline_name):
	global.dialogue_on = false
	if global.game_taken:
		 global.security_done = true
