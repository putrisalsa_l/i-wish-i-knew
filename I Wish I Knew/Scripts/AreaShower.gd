extends Area2D

var button_active = false

func _on_AreaShower_body_entered(body):
	if body.get_name() == 'Chris':
		button_active = true

func _on_AreaShower_body_exited(body):
	if body.get_name() == 'Chris':
		button_active = false

func _input(event):
	if Input.is_action_just_pressed('interact') and button_active:
		button_active = false
		$showerClosed.visible = true
		global.chris_invisible = true
		global.chris_showering = true

func _process(delta):
	$"interactButton".visible = button_active
