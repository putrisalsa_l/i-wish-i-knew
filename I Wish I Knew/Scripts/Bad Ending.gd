extends Node2D

var playscene_dad_dead = false
var dialogue_dad_dead = false

func _ready():
	$AnimationPlayer.play("Fade")
	music.stop_blackout_music()
	playscene_dad_dead = true

func _process(delta):
	if playscene_dad_dead:
		yield($AnimationPlayer, "animation_finished")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == 'Fade':
		dialogue_dad_dead = Dialogic.start('dad dead')
		add_child(dialogue_dad_dead)
		dialogue_dad_dead.connect('timeline_end', self, 'dialogue_end')

func dialogue_end(timeline_name):
	$AnimationPlayer.play("End Game")

func _on_LinkButton_pressed():
#	get_tree().change_scene(str("res://Scenes/Main Menu.tscn"))
	get_tree().quit()
