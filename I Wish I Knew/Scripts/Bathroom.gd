extends Node2D

export (String) var sceneName = ''
var dialogue_stay_inside
var dialogue_go_out
var dialogue_timeout

func _process(delta):
	if global.chris_showering:
		$AnimationPlayer.play("Lamp blinking")
		yield($AnimationPlayer, "animation_finished")
	
	if global.ghost_enter:
		$AnimationPlayer.play("ghost enter")

		global.button_timer_show = true
		if global.button_timer_show:
			$TimerBar.visible = true
			$TimerBar2.visible = true
		else:
			$TimerBar.visible = false
			$TimerBar2.visible = false

		yield($AnimationPlayer, "animation_finished")

	if global.ghost_away:
		$AnimationPlayer.play("ghost away")
		yield($AnimationPlayer, "animation_finished")
	
	if global.change_scene_bedroom_morning:
		$AnimationPlayer.play("Fade")
		yield($AnimationPlayer, "animation_finished")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == 'Lamp blinking':
		global.chris_showering = false
		global.ghost_enter = true

	if anim_name == 'ghost enter':
		$TimerBar.visible = false
		$TimerBar2.visible = false
		global.ghost_enter = false
		global.button_timer_show = false
		global.ghost_away = true
		
	if anim_name == 'ghost away':
		global.ghost_away = false
		$Ghost.visible = false

		if global.buttonstay_pressed:
			dialogue_stay_inside = Dialogic.start('stay inside')
			add_child(dialogue_stay_inside)
			dialogue_stay_inside.connect('timeline_end', self, 'dialogue_end')

		elif global.buttongoout_pressed:
			global.chris_invisible = false
			dialogue_go_out = Dialogic.start('go out')
			add_child(dialogue_go_out)
			dialogue_go_out.connect('timeline_end', self, 'dialogue_end')

		elif global.timeout:
			dialogue_timeout = Dialogic.start('timeout')
			add_child(dialogue_timeout)
			dialogue_timeout.connect('timeline_end', self, 'dialogue_end')

	if anim_name == 'Fade':
		get_tree().change_scene(str('res://Scenes/' + sceneName + '.tscn'))

func dialogue_end(timeline_name):
	global.chris_invisible = false
	global.buttonstay_pressed = false
	global.buttongoout_pressed = false
	global.timeout = false
	global.change_scene_bedroom_morning = true
