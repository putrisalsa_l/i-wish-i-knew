extends Node2D

export (String) var sceneName = ''
var dialogue_must_goto_school

func _ready():
	$AnimationPlayer.play("Fade")
	global.playscene_fade_morning = true
	global.change_scene_bedroom_morning = false
	global.chris_invisible = false

func _process(delta):
	if global.playscene_fade_morning:
		yield($AnimationPlayer, "animation_finished")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == 'Fade':
		dialogue_must_goto_school = Dialogic.start('must go')
		add_child(dialogue_must_goto_school)
		dialogue_must_goto_school.connect('timeline_end', self, 'dialogue_end')

func dialogue_end(timeline_name):
	global.playscene_fade_morning = false
