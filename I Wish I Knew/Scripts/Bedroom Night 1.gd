extends Node2D

export (String) var sceneName = ''
var dialogue_wanna_sleep

func _ready():
	$AnimationPlayer.play("Fade")
	global.playscene_wanna_sleep = true

func _process(delta):
	if global.playscene_wanna_sleep:
		yield($AnimationPlayer, "animation_finished")
	
	if global.playscene_gate_opened:
		$AnimationPlayer.play("Gate_opened")
		yield($AnimationPlayer, "animation_finished")
	
	if global.playscene_lewat:
		$AnimationPlayer.play("Lewat")
		$AnimatedWindow.play("lewat")
		yield($AnimationPlayer, "animation_finished")

	if global.change_scene_bedroom_morning:
		get_tree().change_scene(str('res://Scenes/' + sceneName + '.tscn'))

func dialogue_end(timeline_name):
	global.dialogue_on = false
	global.playscene_wanna_sleep = false

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == 'Fade':
		global.dialogue_on = true
		dialogue_wanna_sleep = Dialogic.start('wanna sleep')
		add_child(dialogue_wanna_sleep)
		dialogue_wanna_sleep.connect('timeline_end', self, 'dialogue_end')

	elif anim_name == 'Gate_opened':
		global.playscene_gate_opened = false
		global.playscene_lewat = true

	elif anim_name == 'Lewat':
		global.playscene_lewat = false
		global.change_scene_bedroom_morning = true
