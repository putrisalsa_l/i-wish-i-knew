extends Node2D

var dialogue_put_books
var dialogue_whats_that
var dialogue_brave_or_not
var dialogue_kill
var dialogue_no_kill

func _ready():
	$AnimationPlayer.play("Door closed")
	global.playscene_put_books = true
	global.kill_pressed = false

func _process(delta):
	if global.playscene_put_books:
		yield($AnimationPlayer, "animation_finished")

	if global.put_books:
		$AnimationPlayer.play("Door sound")
		yield($AnimationPlayer, "animation_finished")

	if global.brave_or_not:
		$AnimationPlayer.play("Brave or Not")
		yield($AnimationPlayer, "animation_finished")

	if global.button_kill_show:
		$TimerBar.visible = true
	
	if global.kill_pressed:
		global.kill_him = true
		get_tree().change_scene(str('res://Scenes/Bad Ending.tscn'))
	if global.timeout:
		global.no_kill_him = true
		get_tree().change_scene(str('res://Scenes/Good Ending.tscn'))

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == 'Door closed':
		global.dialogue_on = true
		dialogue_put_books = Dialogic.start('put books')
		add_child(dialogue_put_books)
		dialogue_put_books.connect('timeline_end', self, 'dialogue_end')

	if anim_name == 'Door sound':
		global.dialogue_on = true
		global.brave_or_not = true
		global.put_books = false
		dialogue_whats_that = Dialogic.start('whats that')
		add_child(dialogue_whats_that)
		dialogue_whats_that.connect('timeline_end', self, 'dialogue_end')

	if anim_name == 'Brave or Not':
		global.dialogue_on = true
		global.button_kill_show = true
		global.brave_or_not = false
		dialogue_brave_or_not = Dialogic.start('brave or not')
		add_child(dialogue_brave_or_not)
		dialogue_brave_or_not.connect('timeline_end', self, 'dialogue_end')

func dialogue_end(timeline_name):
	global.playscene_put_books = false
	global.dialogue_on = false
	global.put_books = false
