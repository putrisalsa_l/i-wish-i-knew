extends KinematicBody2D

var speed = 300
var velocity = Vector2()
var can_move = false

func _ready():
	$AnimatedSprite.play('back with bag')

func _input(event):
	velocity.x = 0
	if Input.is_action_pressed('ui_right'):
		if can_move:
			velocity.x += speed
			$AnimatedSprite.play('walk with bag')
			$AnimatedSprite.flip_h = true
	elif Input.is_action_pressed('ui_left'):
		if can_move:
			velocity.x -= speed
			$AnimatedSprite.play('walk with bag')
			$AnimatedSprite.flip_h = false
	else:
		if can_move:
			$AnimatedSprite.play('stand with bag')

	if global.playscene_fade_morning or global.playscene_letter_from_dad or global.dialogue_on:
		can_move = false
		$AnimatedSprite.play('stand with bag')
	else:
		can_move = true
	
	if global.change_scene_bedroom_morning:
		global.chris_invisible = false

func _process(delta):
	velocity = move_and_slide(velocity)
