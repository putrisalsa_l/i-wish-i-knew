extends KinematicBody2D

var speed = 300
var velocity = Vector2()
var can_move = false

func _ready():
	if global.enter_bathroom:
		$AnimatedSprite.play('stand')
	else:
		$AnimatedSprite.play('stand with bag')

func _input(event):
	velocity.x = 0
	if Input.is_action_pressed('ui_right'):
		if can_move:
			velocity.x += speed
			$AnimatedSprite.flip_h = true
			if global.enter_bathroom:
				$AnimatedSprite.play('walk')
			else:
				$AnimatedSprite.play('walk with bag')

	elif Input.is_action_pressed('ui_left'):
		if can_move:
			velocity.x -= speed
			$AnimatedSprite.flip_h = false
			if global.enter_bathroom:
				$AnimatedSprite.play('walk')
			else:
				$AnimatedSprite.play('walk with bag')
	else:
		if can_move:
			if global.enter_bathroom:
				$AnimatedSprite.play('stand')
			else:
				$AnimatedSprite.play('stand with bag')

	if global.playscene_wanna_shower or global.dialogue_on:
		can_move = false
	elif global.chris_invisible:
		can_move = false
		$AnimatedSprite.visible = false
	elif global.put_books:
		can_move = false
		$AnimatedSprite.play('side')
		$AnimatedSprite.flip_h = true
	else:
		can_move = true
		$AnimatedSprite.visible = true

func _process(delta):
	velocity = move_and_slide(velocity)
