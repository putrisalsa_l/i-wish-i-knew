extends KinematicBody2D

var speed = 300
var velocity = Vector2()
var can_move = false

func _ready():
	$AnimatedSprite.play('back')

func _input(event):
	velocity.x = 0
	if Input.is_action_pressed('ui_right'):
		if can_move:
			velocity.x += speed
			$AnimatedSprite.play('walk')
			$AnimatedSprite.flip_h = true
	elif Input.is_action_pressed('ui_left'):
		if can_move:
			velocity.x -= speed
			$AnimatedSprite.play('walk')
			$AnimatedSprite.flip_h = false
	else:
		if can_move:
			$AnimatedSprite.play('stand')

	if global.playscene_wanna_sleep or global.playscene_lewat or global.dialogue_on:
		can_move = false
		$AnimatedSprite.play('stand')
	else:
		can_move = true

	if global.chris_invisible:
		velocity.x = 0
		$AnimatedSprite.visible = false
		global.playscene_lewat = true
	else:
		$AnimatedSprite.visible = true
	
	if global.change_scene_bedroom_morning:
		global.change_scene_bedroom_morning = false
		global.chris_invisible = false

func _process(delta):
	velocity = move_and_slide(velocity)
