extends Node2D

export (String) var sceneName = ''
var dialogue_dad_texted

func _process(delta):
	if global.security_done:
		$AnimationPlayer.play("Handphone")
		yield($AnimationPlayer, "animation_finished")

func _on_AnimationPlayer_animation_finished(anim_name):
	global.game_taken = false
	global.security_done = false
	if anim_name == 'Handphone':
		global.dialogue_on = true
		dialogue_dad_texted = Dialogic.start('dad texted')
		add_child(dialogue_dad_texted)
		dialogue_dad_texted.connect('timeline_end', self, 'dialogue_end')
	
	if anim_name == 'Fade':
		get_tree().change_scene(str('res://Scenes/' + sceneName + '.tscn'))

func dialogue_end(timeline_name):
	global.dialogue_on = false
	$AnimationPlayer.play("Fade")
	yield($AnimationPlayer, "animation_finished")
