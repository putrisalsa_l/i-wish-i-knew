extends Area2D

var button_active = false
var dialogue_dont_want_bathroom

func _on_AreaBathroom_body_entered(body):
	if body.get_name() == 'Chris':
		button_active = true

func _on_AreaBathroom_body_exited(body):
	if body.get_name() == 'Chris':
		button_active = false

func _input(event):
	if Input.is_action_just_pressed('interact') and button_active:
		global.dialogue_on = true
		button_active = false
		dialogue_dont_want_bathroom = Dialogic.start('dont want bathroom')
		add_child(dialogue_dont_want_bathroom)
		dialogue_dont_want_bathroom.connect('timeline_end', self, 'dialogue_end')

func _process(delta):
	$"interactButton".visible = button_active

func dialogue_end(timeline_name):
	global.dialogue_on = false
	global.playscene_wanna_sleep = false
