extends Node2D

var playscene_dad_alive = false
var dialogue_dad_alive = false

func _ready():
	$AnimationPlayer.play("Fade")
	music.stop_blackout_music()
	playscene_dad_alive = true

func _process(delta):
	if playscene_dad_alive:
		yield($AnimationPlayer, "animation_finished")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == 'Fade':
		dialogue_dad_alive = Dialogic.start('dad alive')
		add_child(dialogue_dad_alive)
		dialogue_dad_alive.connect('timeline_end', self, 'dialogue_end')

func dialogue_end(timeline_name):
	$AnimationPlayer.play("End Game")

func _on_LinkButton_pressed():
#	get_tree().change_scene(str("res://Scenes/Main Menu.tscn"))
	get_tree().quit()
