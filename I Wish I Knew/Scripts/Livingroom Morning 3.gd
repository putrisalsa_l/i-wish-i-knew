extends Node2D

export (String) var sceneName = ''
var dialogue_where_isdad

func _ready():
	$AnimationPlayer.play("Door closed")
	global.enter_bathroom = false
	global.playscene_letter_from_dad = true

func _process(delta):
	if global.playscene_letter_from_dad:
		yield($AnimationPlayer, "animation_finished")

	if global.change_scene_school_evening:
		$AnimationPlayer.play("Fade")
		yield($AnimationPlayer, "animation_finished")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == 'Door closed':
		global.dialogue_on = true
		dialogue_where_isdad = Dialogic.start('where is dad')
		add_child(dialogue_where_isdad)
		dialogue_where_isdad.connect('timeline_end', self, 'dialogue_end')

	if anim_name == 'Fade':
		get_tree().change_scene(str('res://Scenes/' + sceneName + '.tscn'))

func dialogue_end(timeline_name):
	global.playscene_letter_from_dad = false
	global.dialogue_on = false
