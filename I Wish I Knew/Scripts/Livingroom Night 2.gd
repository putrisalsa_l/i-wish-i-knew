extends Node2D

export (String) var sceneName = ''
var dialogue_wanna_shower

func _ready():
	$AnimationPlayer.play("Door closed")
	music.stop_town_music()
	global.playscene_wanna_shower = true

func _process(delta):
	if global.playscene_wanna_shower:
		yield($AnimationPlayer, "animation_finished")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == 'Door closed':
		global.dialogue_on = true
		dialogue_wanna_shower = Dialogic.start('wanna shower')
		add_child(dialogue_wanna_shower)
		dialogue_wanna_shower.connect('timeline_end', self, 'dialogue_end')

func dialogue_end(timeline_name):
	global.playscene_wanna_shower = false
	global.dialogue_on = false
