extends Node2D

var dialogue_blackout

func _ready():
	$AnimationPlayer.play("Blackout")
	music.stop_town_music()
	global.playscene_blackout = true
	music.play_blackout_music()

func _process(delta):
	if global.playscene_blackout:
		yield($AnimationPlayer, "animation_finished")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == 'Blackout':
		global.dialogue_on = true
		dialogue_blackout = Dialogic.start('blackout')
		add_child(dialogue_blackout)
		dialogue_blackout.connect('timeline_end', self, 'dialogue_end')

func dialogue_end(timeline_name):
	global.playscene_blackout = false
	global.dialogue_on = false
	global.blackout = true
