extends LinkButton

export(String) var scene_to_load

func _on_PlayButton_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))

func _on_ExitButton_pressed():
	get_tree().quit()
