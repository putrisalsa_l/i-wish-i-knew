extends Node2D

var dialogue_help_security

func _ready():
	$AnimationPlayer.play("Fade")
	music.play_town_music()
	global.playscene_help_security = true
	global.change_scene_school_evening = false

func _process(delta):
	if global.playscene_help_security:
		yield($AnimationPlayer, "animation_finished")

func dialogue_end(timeline_name):
	global.playscene_help_security = false
	global.dialogue_on = false

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == 'Fade':
		global.dialogue_on = true
		dialogue_help_security = Dialogic.start('help security')
		add_child(dialogue_help_security)
		dialogue_help_security.connect('timeline_end', self, 'dialogue_end')
