extends Node2D

var dialogue_want_medicine

func _ready():
	$AnimationPlayer.play("Fade")
	music.play_town_music()
	global.playscene_want_medicine = true
	global.change_scene_school_evening = false

func _process(delta):
	if global.playscene_want_medicine:
		yield($AnimationPlayer, "animation_finished")

func dialogue_end(timeline_name):
	global.playscene_want_medicine = false
	global.dialogue_on = false

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == 'Fade':
		global.dialogue_on = true
		dialogue_want_medicine = Dialogic.start('medicine')
		add_child(dialogue_want_medicine)
		dialogue_want_medicine.connect('timeline_end', self, 'dialogue_end')
