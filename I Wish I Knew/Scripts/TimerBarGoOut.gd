extends Line2D

var button_pressed = false
var time_out = false

func timer_decrease():
	points[1].x -= 10

func _on_Timer_timeout():
	if global.button_timer_show:
		$Timer.start()
		timer_decrease()
	
	if points[1].x == 0:
		global.timeout = true
		$LinkButton.visible = false
		$Timer.stop()
	elif button_pressed or global.buttonstay_pressed:
		$LinkButton.visible = false
		$Timer.stop()

func _on_LinkButton_pressed():
	button_pressed = true
	global.buttongoout_pressed = true
