extends Node2D

var town_music = load("res://Assets/Sound/at-town.wav")
var blackout = load("res://Assets/Sound/blackout.wav")

func play_town_music():
	$TownMusic.stream = town_music
	$TownMusic.play()

func stop_town_music():
	$TownMusic.stop()

func play_blackout_music():
	$Blackout.stream = blackout
	$Blackout.play()

func stop_blackout_music():
	$Blackout.stop()
