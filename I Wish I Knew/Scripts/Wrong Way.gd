extends Area2D

var dialogue_wrong_way

func _on_wrong_way_body_entered(body):
	if body.get_name() == 'Chris':
		global.dialogue_on = true
		dialogue_wrong_way = Dialogic.start('wrong way')
		add_child(dialogue_wrong_way)
		dialogue_wrong_way.connect('timeline_end', self, 'dialogue_end')

func dialogue_end(timeline_name):
	global.dialogue_on = false
