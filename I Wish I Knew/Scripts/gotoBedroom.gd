extends Area2D

export (String) var sceneName = ''

func _on_gotoBedroom_body_entered(body):
	if body.get_name() == 'Chris' and global.done_shower:
		get_tree().change_scene(str('res://Scenes/' + sceneName + '.tscn'))

	elif body.get_name() == 'Chris' and global.blackout:
		get_tree().change_scene(str('res://Scenes/' + sceneName + '.tscn'))
