extends Area2D

export (String) var sceneName = ''

func _on_gotoLivingroom_body_entered(body):
	if body.get_name() == 'Chris':
		get_tree().change_scene(str('res://Scenes/' + sceneName + '.tscn'))
